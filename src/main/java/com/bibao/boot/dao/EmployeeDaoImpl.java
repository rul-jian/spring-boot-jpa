package com.bibao.boot.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bibao.boot.entity.EmployeeEntity;

@Repository
@Transactional
public class EmployeeDaoImpl implements EmployeeDao {
	@PersistenceContext
	private EntityManager em;

	@Override
	public void save(EmployeeEntity entity) {
		em.persist(entity);
	}

	@Override
	public void update(EmployeeEntity entity) {
		em.merge(entity);
	}

	@Override
	public void deleteById(int id) {
		String query = "delete EmployeeEntity e where e.id = :id";
		em.createQuery(query).setParameter("id", id).executeUpdate();
	}

	@Override
	public EmployeeEntity findById(int id) {
		return em.find(EmployeeEntity.class, id);
	}

	@Override
	public EmployeeEntity findByName(String name) {
		String query = "select e from EmployeeEntity e where e.name = :name";
		return em.createQuery(query, EmployeeEntity.class)
				.setParameter("name", name)
				.getSingleResult();
	}

	@Override
	public List<EmployeeEntity> findAll() {
		String query = "select e from EmployeeEntity e";
		return em.createQuery(query, EmployeeEntity.class).getResultList();
	}

}
