package com.bibao.boot.dao;

import java.util.List;

import com.bibao.boot.entity.EmployeeEntity;

public interface EmployeeDao {
	public void save(EmployeeEntity entity);
	public void update(EmployeeEntity entity);
	public void deleteById(int id);
	public EmployeeEntity findById(int id);
	public EmployeeEntity findByName(String name);
	public List<EmployeeEntity> findAll();
}
