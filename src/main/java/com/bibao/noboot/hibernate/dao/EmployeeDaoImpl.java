package com.bibao.noboot.hibernate.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.bibao.boot.entity.EmployeeEntity;

@Repository
@Transactional
public class EmployeeDaoImpl implements EmployeeDao {
	private HibernateTemplate template;
	
	@Autowired
	public void setTemplate(SessionFactory factory) {
		template = new HibernateTemplate(factory);
	}
	
	@Override
	public void save(EmployeeEntity entity) {
		template.save(entity);
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public EmployeeEntity findByName(String name) {
		String query = "select e from EmployeeEntity e where e.name = :name";
		List<EmployeeEntity> entities = (List<EmployeeEntity>)template.findByNamedParam(query, "name", name);
		if (CollectionUtils.isEmpty(entities)) return null;
		return entities.get(0);
 	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<EmployeeEntity> findAll() {
		String query = "select e from EmployeeEntity e";
		return (List<EmployeeEntity>)template.find(query);
	}

}
