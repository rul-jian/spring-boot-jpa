package com.bibao.noboot.jdbc.dao;

import java.util.List;

import com.bibao.noboot.jdbc.model.Employee;

public interface EmployeeDao {
	public void save(Employee emp);
	public Employee findByName(String name);
	public List<Employee> findAll();
}
