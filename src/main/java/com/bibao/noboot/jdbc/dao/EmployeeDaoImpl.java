package com.bibao.noboot.jdbc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.bibao.noboot.jdbc.model.Employee;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {
	private JdbcTemplate template;
	
	@Autowired
	public void setTemplate(DataSource dataSource) {
		template = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void save(Employee emp) {
		String query = "insert into employee values(employee_seq.nextval, ?, ?, ?)";
		template.update(query, emp.getName(), emp.getSalary(), emp.getBirthday());
	}

	@Override
	public Employee findByName(String name) {
		String query = "select * from employee where name = ?";
		Object[] params = {name};
		List<Employee> list = template.query(query, params, new RowMapper<Employee>() {
			@Override
			public Employee mapRow(ResultSet rs, int line) throws SQLException {
				Employee emp = new Employee();
				emp.setId(rs.getInt("id"));
				emp.setName(rs.getString("name"));
				emp.setSalary(rs.getInt("salary"));
				emp.setBirthday(rs.getDate("birthday"));
				return emp;
			}
			
		});
		if (CollectionUtils.isEmpty(list)) return null;
		return list.get(0);
	}

	@Override
	public List<Employee> findAll() {
		String query = "select * from employee";
		return template.query(query, new RowMapper<Employee>() {
			@Override
			public Employee mapRow(ResultSet rs, int line) throws SQLException {
				Employee emp = new Employee();
				emp.setId(rs.getInt("id"));
				emp.setName(rs.getString("name"));
				emp.setSalary(rs.getInt("salary"));
				emp.setBirthday(rs.getDate("birthday"));
				return emp;
			}
			
		});
	}

}
