drop table employee;

drop sequence employee_seq;

create table employee (
	id number(3) primary key,
	name varchar2(20) not null,
	salary number(7),
	birthday date
);

create sequence employee_seq
minvalue 1
start with 1;